
# MPI-Epoch Test for User Container CI 

MPI- EPOCH Tests based on simplified TNSA Plasma Simulation

## Installation
The tests are based on Fortran 90 pre-compiled 2D simplified versions of the Plasma Simulation Code 
[Epoch](https://cfsa-pmw.warwick.ac.uk/EPOCH/epoch).

## Directory structure

The Directory Structure is as following: 

```bash

|-- README.md
|-- bin
|   |-- epoch2d_gcc8
|   `-- epoch2d_i17
|-- clean_logs.sh
|-- data_gcc8
|   `-- input.deck -> ../def.tnsa
|-- data_i17
|   `-- input.deck -> ../def.tnsa
|-- def.tnsa
|-- install.sh
|-- logs
|   |-- compile_gcc.log
|   `-- compile_icc.log
|-- lwfa_gcc8.sh
|-- lwfa_it.sh
|-- mpi_epoch2d_gcc.sh
|-- mpi_epoch2d_icc.sh
|-- mpi_epoch2d_tests.sh
|-- scripts
|   |-- sub_gcc_test.sh
|   `-- sub_icc_test.sh
`-- src (epoch sources)
    
```

The Fortran executables are located in the /bin directory for 2 type of compilers
   - GNU GCC 8.1.0
   - Intel ICC v17.4

## Install from source
If needed, one can install executables for both GNU GCC and Intel ICC compiler using one command from
the top directory:

```bash
./install.sh

```

Compiling process logs can be found in ``` /logs ``` directory

```bash
|-- logs
|   |-- compile_gcc.log
|   `-- compile_icc.log

```

## Usage

One can run both tests (gcc and intel ) using the command:

```bash
./mpi_epoch2d_tests.sh <n_cores>

```

or run separate test for GCC and Intel using the following dedicated scripts:

GCC only:
```bash
./mpi_epoch2d_gcc.sh <n_cores>
```
Intel only:
```bash
./mpi_epoch2d_icc.sh <n_cores>
```

## Shell Exit code
All tests shells exit with error code. By convention, an 'exit 0' indicates success,
 while a non-zero exit value means an error or anomalous condition.

Non-zero return code follow the convention :


|  Error Type       | Exit Code  |
|:-----------------:|:----------:| 
| Warnings          |    001     |
| General Errors    |    010     |
| MPI Errors        |    100     |    

The scripts  add these  specific error code so that, for example, error code 
``` 101  ``` means that the tests has created both warnings and MPI related errors.


## Excluding specific nodes

It could Useful sometimes to check for unhomogeneous behaviour of MPI 
on specific nodes.
For that one can exclude a list of nodes using the command:

```bash
./mpi_epoch2d_tests.sh <n_cores> <list_of_excluded_nodes>
```
or, as well for dedicated tests:

```bash
./mpi_epoch2d_tests.sh <n_cores> <list_of_excluded_nodes>
./mpi_epoch2d_tests.sh <n_cores> <list_of_excluded_nodes>

```

Example:

```bash
./mpi_epoch2d_test.sh 128 lxbk[0622-0627]

```

## Cleanup

One can  clean all produced log files, issueing the command:
```bash
./clean_logs.sh
```

 

