#!/bin/bash

#
#         MPI-Epoch TNSA Test for  GCC / Intel compilers 
#
#         D.Bertini 
#         Date 4 Jun 2020


cleanup(){
    TWD=${PWD}
    cd ${TWD}/data_gcc8 && rm -f *.log && cd ..
    rm -f ${TWD}/logs/diagnostics_gcc.log
}


show(){
    local id_gcc=$1
    local nodes_gcc=$2
    log=".err.log"
    diag_log=${PWD}/logs/diagnostics_gcc.log

    echo " "  >> ${diag_log} 
    echo "--------- GnuCC diagnostics ---------------------------------"  >> $diag_log
    echo " " >> $diag_log 

    echo "Epoch-gcc ran on nodes-> $nodes_gcc" >>  $diag_log 
    output_g=$(egrep -cwi 'warning' ./data_gcc8/$id_gcc$log)
    echo "Warnings ...$output_g" >>  $diag_log 
    output_eg=$(egrep -cwi 'error|errors|critical' ./data_gcc8/$id_gcc$log)
    echo "General Errors ...$output_eg" >>  $diag_log 
    output_mg=$(egrep -cwi 'libibverbs|mca|mpi' ./data_gcc8/$id_gcc$log)
    echo "Mpi-related Errors ...$output_mg" >>  $diag_log 

    res=0
    # return values
    if [ $output_g -gt 0 ]; then
	res=1  
    fi 

    if [ $output_eg -gt 0 ]; then 
	res=$((10 + $res))
    fi 

    if [ $output_mg -gt 0 ]; then
	res=$((100 + $res))
    fi 

    echo $res
    return $res
}



cleanup


if [ -n "$1" ]; then
    NTASKS=$1  
else
    NTASKS=32  
fi


if [ -n "$2" ]; then
Command_gcc="sbatch --exclude $2 --job-name ep2d-gcc8 --ntasks ${NTASKS} --ntasks-per-core 1 --partition main --time 0-00:10:00 -D ./data_gcc8 -o %j.out.log -e %j.err.log --exclusive --export ALL -- ${PWD}/lwfa_gcc8.sh"
else
Command_gcc="sbatch  --job-name ep2d-gcc8 --ntasks ${NTASKS} --ntasks-per-core 1 --partition main --time 0-00:10:00 -D ./data_gcc8 -o %j.out.log -e %j.err.log --exclusive --export ALL -- ${PWD}/lwfa_gcc8.sh"
fi


Submit_Output_gcc="$($Command_gcc 2>&1)"
JobId_gcc=`echo $Submit_Output_gcc | grep 'Submitted batch job' | awk '{print $4}'`
echo "Epoch2d_gcc_test jobId--->" ${JobId_gcc}

# Wait for slurm to wake-up our jobs
sleep 5

Host_gcc=`scontrol show job ${JobId_gcc} | grep ' NodeList' | awk -F'=' '{print $2}'`
echo "Epoch2d_gcc test submitted on nodes --->" ${Host_gcc}



while :
do
    now="$(date +"%r")"
    echo " "
    echo " checking job state at $now "
    echo " "
    Js_g=`scontrol show job ${JobId_gcc} | grep ' JobState' | awk -F'=' '{print $2}'`  

    run_g=1
    finished=0
    if [[ $Js_g == *"RUNNING"* ]]; then
	echo " mpi_gcc test is running ..."
    else
	run_g=0
    fi
    if (( run_g==0 )); then
        echo ' Jobs finished '  
	finished=1;
        code=$(show  $JobId_gcc $Host_gcc)
        break   
    fi
    sleep 50
done

res=$(echo $code | awk '{print $NF}')
echo "Test returns code: " $res

#  We return code to shell.
#  By convention, an 'exit 0' indicates success,
#  while a non-zero exit value means an error or anomalous condition.

exit $res
