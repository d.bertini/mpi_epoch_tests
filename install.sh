# Small Epoch installer script for both gcc and intel compiler
#    <d.bertini@gsi.de>

TWD=${PWD}

# save preloaded modules

MODS=${LOADEDMODULES}
all_modules="${MODS//:/" " }"


# GCC Compiler & openMPI 3.1 container version
rm -rf $TWD/logs/*.log
rm -rf $TWD/bin/epoch2d*

GCCLOG_FILE=$TWD/logs/compile_gcc.log
ICCLOG_FILE=$TWD/logs/compile_icc.log

echo "-I- compiling with gcc compiler" 
type mpicc
type mpif90
type mpirun


export COMPILER=gfortran
cd $TWD/src/epoch2d && make clean && (make -j 10 2>&1 | tee ${GCCLOG_FILE}) && cd $TWD
cp $TWD/src/epoch2d/bin/epoch2d $TWD/bin/epoch2d_gcc8

# Intel Compiler and openMPI 4.0 version
# Careful to purge since module are pre_installed on the container
module purge
module use /cvmfs/it.gsi.de/modulefiles
module load openmpi/intel/4.0_intel17.4

echo "-I- compiling with Intel compiler" 
type mpicc
type mpif90
type mpirun

export COMPILER=intel
cd $TWD/src/epoch2d && make clean && (make -j 10 2>&1 | tee ${ICCLOG_FILE}) && cd $TWD
cp $TWD/src/epoch2d/bin/epoch2d $TWD/bin/epoch2d_i17

# clean all builds
cd $TWD/src/epoch2d && make clean


module purge
module load $all_modules

