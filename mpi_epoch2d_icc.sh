#!/bin/bash

#
#         MPI-Epoch TNSA Test for  GCC / Intel compilers 
#
#         D.Bertini 
#         Date 4 Jun 2020


cleanup(){
    TWD=${PWD}
    cd ${TWD}/data_i17  && rm -f *.log && cd ..
    rm -f ${TWD}/logs/diagnostics_icc.log
}


show(){
    local id_icc=$1
    local nodes_icc=$2
    log=".err.log"
    diag_log=${PWD}/logs/diagnostics_icc.log

    echo " " >> $diag_log 
    echo "--------- Intel diagnostics ---------------------------------" >> $diag_log 
    echo " " >> $diag_log 
    echo "Epoch-icc ran on nodes-> $nodes_icc " >> $diag_log 
    output_i=$(egrep -cwi 'warning' ./data_i17/$id_icc$log)
    echo "Warnings ...$output_i" >> $diag_log 
    output_ei=$(egrep -cwi 'error|errors|critical' ./data_i17/$id_icc$log)
    echo "General Errors ...$output_ei" >> $diag_log 
    output_mi=$(egrep -cwi 'libibverbds|mca|mpi' ./data_i17/$id_icc$log)
    echo "Mpi-related Errors ...$output_mi" >> $diag_log 
        
    res=0
    # return values
    if [ $output_i -gt 0 ]; then
	res=1  
    fi 

    if [ $output_ei -gt 0 ]; then 
	res=$((10 + $res))
    fi 

    if [ $output_mi -gt 0 ]; then
	res=$((100 + $res))
    fi 

    echo $res
    return $res
}



cleanup


if [ -n "$1" ]; then
    NTASKS=$1  
else
    NTASKS=32  
fi



if [ -n "$2" ]; then
Command_intel="sbatch --exclude $2 --job-name ep2d-i17 --ntasks ${NTASKS} --ntasks-per-core 1 --partition main --time 0-00:10:00 -D ./data_i17 -o %j.out.log -e %j.err.log --exclusive --export ALL -- ${PWD}/lwfa_it.sh"
else
Command_intel="sbatch  --job-name ep2d-i17 --ntasks ${NTASKS} --ntasks-per-core 1 --partition main --time 0-00:10:00 -D ./data_i17 -o %j.out.log -e %j.err.log --exclusive --export ALL -- ${PWD}/lwfa_it.sh"
fi



Submit_Output_intel="$($Command_intel 2>&1)"
JobId_i=`echo $Submit_Output_intel | grep 'Submitted batch job' | awk '{print $4}'`
echo "Epoch2d_intel_test jobId--->" ${JobId_i}

# Wait for slurm to wake-up our jobs
sleep 5

Host_i=`scontrol show job ${JobId_i} | grep ' NodeList' | awk -F'=' '{print $2}'`
echo "Epoch2d_intel test submitted on nodes --->" ${Host_i}


while :
do
    now="$(date +"%r")"
    echo " "
    echo " checking job state at $now "
    echo " "
    Js_i=`scontrol show job ${JobId_i} | grep ' JobState' | awk -F'=' '{print $2}'`

    run_i=1
    finished=0
    if [[ $Js_i == *"RUNNING"* ]]; then
	echo " mpi_intel test is running ... "
    else
	run_i=0 
    fi
    if (( run_i==0 )); then
        echo ' Jobs finished '  
	finished=1;
        code=$(show  $JobId_i $Host_i)
        break   
    fi
    sleep 50
done

res=$(echo $code | awk '{print $NF}')

echo " Test returns code: "  $res

#  We return code to shell.
#  By convention, an 'exit 0' indicates success,
#  while a non-zero exit value means an error or anomalous condition.
exit $res
