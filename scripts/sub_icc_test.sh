rm -rf ${PWD}/data_i17/*.log
sum=$(($1))
sbatch --ntasks $sum --ntasks-per-core 1 --partition main --time 7-00:00:00 -D ./data_i17 -o %j.out.log -e %j.err.log --exclusive --export ALL -- ${PWD}/lwfa_it.sh
