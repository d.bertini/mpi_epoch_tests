#!/bin/bash

module use /cvmfs/it.gsi.de/modulefiles

module load openmpi/intel/4.0_intel17.4

export OMPI_MCA_btl_openib_allow_ib=true

echo $LD_LIBRARY_PATH

export EPOCH_ROOT=${PWD}/../bin


echo "." | srun  -- $EPOCH_ROOT/epoch2d_i17

